Class {
	#name : #'FileSystem_',
	#superclass : #Object,
	#instVars : [
		'name',
		'createdAt'
	],
	#category : #'FileSystem-Model'
}

{ #category : #accessing }
FileSystem_ >> createdAt [
	^ createdAt
]

{ #category : #accessing }
FileSystem_ >> createdAt: anObject [
	createdAt := anObject
]

{ #category : #accessing }
FileSystem_ >> kBytes [
	self subclassResponsibility
]

{ #category : #accessing }
FileSystem_ >> name [
	^ name
]

{ #category : #accessing }
FileSystem_ >> name: anObject [
	name := anObject
]
