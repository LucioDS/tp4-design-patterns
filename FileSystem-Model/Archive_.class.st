Class {
	#name : #'Archive_',
	#superclass : #'FileSystem_',
	#instVars : [
		'kBytes'
	],
	#category : #'FileSystem-Model'
}

{ #category : #accessing }
Archive_ class >> called: aString createdAt: aDateAndTime kBytes: anInt [
	^ self new
		name: aString;
		kBytes:  anInt;
		createdAt: aDateAndTime;
		yourself
]

{ #category : #accessing }
Archive_ >> kBytes [
	^ kBytes
]

{ #category : #accessing }
Archive_ >> kBytes: anObject [
	kBytes := anObject
]

{ #category : #accessing }
Archive_ >> path [
^self name
]
