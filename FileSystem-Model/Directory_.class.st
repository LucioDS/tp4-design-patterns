Class {
	#name : #'Directory_',
	#superclass : #'FileSystem_',
	#instVars : [
		'archives'
	],
	#category : #'FileSystem-Model'
}

{ #category : #initialization }
Directory_ >> addArchive: anArchive_ [
	archives add: anArchive_ 
]

{ #category : #initialization }
Directory_ >> addArchives: aCollectionOfArchives_ [
	archives addAll: aCollectionOfArchives_
]

{ #category : #accessing }
Directory_ >> archives [
	^ archives
]

{ #category : #accessing }
Directory_ >> archives: anObject [
	archives := anObject
]

{ #category : #initialization }
Directory_ >> biggestArchive [
	^ (self archives collect: [ :file | file kBytes ]) max
]

{ #category : #initialization }
Directory_ >> contentList [
	^ self archives collect: [ :arch | '/' , arch path ]
]

{ #category : #initialization }
Directory_ >> initialize [
	self
		archives: OrderedCollection new
]

{ #category : #initialization }
Directory_ >> kBytes [
	^ self archives inject: 0 into: [ :sum :arch | sum + arch kBytes ]
]

{ #category : #initialization }
Directory_ >> path [
	^ self archives
		inject: ''
		into: [ :base :arch | base , arch path  ]
]

{ #category : #initialization }
Directory_ >> totalOccupiedSize [
	^ self kBytes
]
