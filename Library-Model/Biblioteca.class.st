"
.
"
Class {
	#name : #Biblioteca,
	#superclass : #Object,
	#instVars : [
		'socios',
		'exporter'
	],
	#category : #'Library-Model'
}

{ #category : #'as yet unclassified' }
Biblioteca class >> new: anExporter [
	^ self new initialize: anExporter
]

{ #category : #socios }
Biblioteca >> agregarSocio: arg1 [
	socios add: arg1
]

{ #category : #exporting }
Biblioteca >> exportarSocios [
	^ self exporter export: self socios
]

{ #category : #accessing }
Biblioteca >> exporter [
	^ exporter
]

{ #category : #accessing }
Biblioteca >> exporter: arg1 [
	exporter := arg1
]

{ #category : #initialization }
Biblioteca >> initialize [
	socios := OrderedCollection new.
	exporter := JSON new
]

{ #category : #initialization }
Biblioteca >> initialize: arg1 [
	socios := OrderedCollection new.
	exporter := arg1
]

{ #category : #accessing }
Biblioteca >> socios [
	^ socios
]

{ #category : #accessing }
Biblioteca >> socios: arg1 [
	socios := arg1
]
