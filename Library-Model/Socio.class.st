"
.
"
Class {
	#name : #Socio,
	#superclass : #Object,
	#instVars : [
		'nombre',
		'email',
		'legajo'
	],
	#category : #'Library-Model'
}

{ #category : #creation }
Socio class >> nombre: arg1 email: arg2 legajo: arg3 [
	^ self new nombre: arg1 email: arg2 legajo: arg3
]

{ #category : #accessing }
Socio >> email [
	^ email
]

{ #category : #accessing }
Socio >> email: arg1 [
	email := arg1
]

{ #category : #accessing }
Socio >> legajo [
	^ legajo
]

{ #category : #accessing }
Socio >> legajo: arg1 [
	legajo := arg1
]

{ #category : #accessing }
Socio >> nombre [
	^ nombre
]

{ #category : #accessing }
Socio >> nombre: arg1 [
	nombre := arg1
]

{ #category : #initialization }
Socio >> nombre: arg1 email: arg2 legajo: arg3 [
	nombre := arg1.
	email := arg2.
	legajo := arg3
]
