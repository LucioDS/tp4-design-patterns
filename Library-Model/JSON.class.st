"
This class implements the methods to export socios in JSON format 

note: 

remembet to install the dependency

```
Metacello new
repository: 'github://svenvc/NeoJSON/repository';
baseline: 'NeoJSON';
load.
```
"
Class {
	#name : #JSON,
	#superclass : #Exporter,
	#category : #'Library-Model'
}

{ #category : #exporting }
JSON >> export: socios [
	^ NeoJSONWriter toStringPretty: (self exportAllToDictionary: socios)
]
