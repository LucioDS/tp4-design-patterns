"
.
"
Class {
	#name : #Exporter,
	#superclass : #Object,
	#category : #'Library-Model'
}

{ #category : #exporting }
Exporter >> export: socios [
	self subclassResponsibility
]

{ #category : #exporting }
Exporter >> exportAllToDictionary: socios [
	^ socios collect: [ :socio | self exportToDictionary: socio ]
]

{ #category : #exporting }
Exporter >> exportToDictionary: socio [
	| socioDictionary |
	socioDictionary := Dictionary new.
	socioDictionary
		at: #legajo put: socio legajo;
		at: #email put: socio email;
		at: #nombre put: socio nombre.
	^ socioDictionary yourself
]
