"
This class contains tests for JSON class
"
Class {
	#name : #JSONtest,
	#superclass : #TestCase,
	#instVars : [
		'expected',
		'socios',
		'exporter',
		's1',
		's2'
	],
	#category : #'Library-Tests'
}

{ #category : #running }
JSONtest >> setUp [
	"Hooks that subclasses may override to define the fixture of test."
s1 := Socio
		nombre: 'Arya Stark'
		email: 'needle@stark.com'
		legajo: '5234/5'.
	s2 := Socio
		nombre: 'Tyron Lannister'
		email: 'tyron@thelannisters.com'
		legajo: '2345/2'.
	socios := OrderedCollection with: s1 with: s2.
	
	exporter := JSON new
]

{ #category : #tests }
JSONtest >> testExport [
	"as we have a dependency we can chech that the method is executed with the correct data an asume that works"

	"self assert: NeoJSONWriter isCalled: 'toStringPretty:' "
	self assert: (exporter export: socios) class equals: ByteString
]
