"
This class contains tests for exporter abstract class in Library package
"
Class {
	#name : #ExporterTest,
	#superclass : #TestCase,
	#instVars : [
		'socios',
		'exporter',
		's1',
		's2',
		'dictionaries'
	],
	#category : #'Library-Tests'
}

{ #category : #running }
ExporterTest >> setUp [
	s1 := Socio
		nombre: 'Arya Stark'
		email: 'needle@stark.com'
		legajo: '5234/5'.
	s2 := Socio
		nombre: 'Tyron Lannister'
		email: 'tyron@thelannisters.com'
		legajo: '2345/2'.
	socios := OrderedCollection with: s1 with: s2.
	exporter := JSON new
]

{ #category : #tests }
ExporterTest >> testExportAllToDictionary [
	dictionaries := exporter exportAllToDictionary: socios.
	self
		assert: (dictionaries first includesKey: #email);
		assert: (dictionaries first includesKey: #nombre);
		assert: (dictionaries first includesKey: #legajo);
		assert: (dictionaries second includesKey: #email);
		assert: (dictionaries second includesKey: #nombre);
		assert: (dictionaries second includesKey: #legajo)
]

{ #category : #tests }
ExporterTest >> testExportToDictionary [
	self
		assert: ((exporter exportToDictionary: socios first) includes: s1 nombre);
		assert: ((exporter exportToDictionary: socios first) includes: s1 email);
		assert: ((exporter exportToDictionary: socios first) includes: s1 legajo).
	self
		assert: ((exporter exportToDictionary: socios second) includes: s2 nombre);
		assert: ((exporter exportToDictionary: socios second) includes: s2 email);
		assert: ((exporter exportToDictionary: socios second) includes: s2 legajo)
]
