Class {
	#name : #Topography,
	#superclass : #Object,
	#category : #'Topography-Model'
}

{ #category : #abstract }
Topography >> proportion [
	^ self subclassResponsibility
]
