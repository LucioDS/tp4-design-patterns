Class {
	#name : #Water,
	#superclass : #Topography,
	#category : #'Topography-Model'
}

{ #category : #abstract }
Water >> proportion [
	^ 1
]
