Class {
	#name : #Mix,
	#superclass : #Topography,
	#instVars : [
		'topographies'
	],
	#category : #'Topography-Model'
}

{ #category : #'instance creation' }
Mix class >> newWith: topographiesCollection [
	^ self
		new
		addTopographies: topographiesCollection
]

{ #category : #adding }
Mix >> addTopographies: aTopographiesCollection [
	topographies addAll: aTopographiesCollection
]

{ #category : #adding }
Mix >> addTopography: aTopography [
	topographies add: aTopography
]

{ #category : #count }
Mix >> equalProportion: aTopography [
	^ self proportion = aTopography proportion
]

{ #category : #adding }
Mix >> initialize [
	self topographies: OrderedCollection new
]

{ #category : #count }
Mix >> proportion [
	^ (topographies 
		inject: 0
		into: [ :sum :topology | sum + topology proportion ])
		/ topographies  size
]

{ #category : #accessing }
Mix >> topographies [
	^ topographies 
]

{ #category : #accessing }
Mix >> topographies:topographiesColl [
	topographies := topographiesColl
]
