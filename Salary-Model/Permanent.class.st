Class {
	#name : #Permanent,
	#superclass : #Employee,
	#instVars : [
		'laborOld'
	],
	#category : #'Salary-Model'
}

{ #category : #'instance creation' }
Permanent class >> newWith: amountOfChilds laborOld: amountOfYears isMarried: aBoolean [
	^ self new 
	  amountChilds: amountOfChilds;
	  laborOld: amountOfYears;
		isMarried: aBoolean;
		yourself
]

{ #category : #calculating }
Permanent >> aditional [
	^ self marriedBonus + (100 * self amountChilds) + (100 * self laborOld)
]

{ #category : #calculating }
Permanent >> basic [
	^ 3000
]

{ #category : #accessing }
Permanent >> laborOld [
	^ laborOld
]

{ #category : #accessing }
Permanent >> laborOld: anObject [
	laborOld := anObject
]
