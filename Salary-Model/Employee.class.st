Class {
	#name : #Employee,
	#superclass : #Object,
	#instVars : [
		'isMarried',
		'amountChilds'
	],
	#category : #'Salary-Model'
}

{ #category : #accessing }
Employee >> aditional [
	^ self subclassResponsibility
]

{ #category : #accessing }
Employee >> amountChilds [
	^ amountChilds
]

{ #category : #accessing }
Employee >> amountChilds: anObject [
	amountChilds := anObject
]

{ #category : #accessing }
Employee >> basic [
	^self subclassResponsibility 
]

{ #category : #calculating }
Employee >> deductions [
	^ (self basic * 0.13) + (self aditional * 0.5)
]

{ #category : #accessing }
Employee >> isMarried [
	^ isMarried
]

{ #category : #accessing }
Employee >> isMarried: anObject [
	isMarried := anObject
]

{ #category : #accessing }
Employee >> marriedBonus [
	self isMarried
		ifTrue: [ ^ 500].
		^0
]

{ #category : #computing }
Employee >> salary [
	^ self basic + self aditional - self deductions
]
