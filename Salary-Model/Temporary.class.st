Class {
	#name : #Temporary,
	#superclass : #Employee,
	#instVars : [
		'hoursWorked'
	],
	#category : #'Salary-Model'
}

{ #category : #'instance creation' }
Temporary class >> newWith: amountHoursWorked childs: amountOfChilds  isMarried: aBoolean [
	^ self new
		hoursWorked: amountHoursWorked ;
		amountChilds: amountOfChilds ;
		isMarried: aBoolean;
		yourself
]

{ #category : #calculating }
Temporary >> aditional [
	^self marriedBonus + (100 * self amountChilds )
]

{ #category : #calculating }
Temporary >> basic [
	^ 4000 + (10 * self hoursWorked)
]

{ #category : #accessing }
Temporary >> hoursWorked [
	^ hoursWorked
]

{ #category : #accessing }
Temporary >> hoursWorked: anObject [
	hoursWorked := anObject
]
