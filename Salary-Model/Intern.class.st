Class {
	#name : #Intern,
	#superclass : #Employee,
	#instVars : [
		'examsDone'
	],
	#category : #'Salary-Model'
}

{ #category : #'instance creation' }
Intern class >> newWith: amountOfExamsDone [
	^self new examsDone: amountOfExamsDone.
]

{ #category : #calculating }
Intern >> aditional [
	^ 100 * self examsDone 
]

{ #category : #calculating }
Intern >> basic [
	^ 2000
]

{ #category : #accessing }
Intern >> examsDone [
	^ examsDone
]

{ #category : #accessing }
Intern >> examsDone: anObject [
	examsDone := anObject
]
