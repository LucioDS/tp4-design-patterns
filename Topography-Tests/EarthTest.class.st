Class {
	#name : #EarthTest,
	#superclass : #TestCase,
	#instVars : [
		'earth'
	],
	#category : #'Topography-Tests'
}

{ #category : #running }
EarthTest >> setUp [ 
	earth := Earth new
]

{ #category : #running }
EarthTest >> testProportion [ 
	self assert: earth proportion equals: 0
]
