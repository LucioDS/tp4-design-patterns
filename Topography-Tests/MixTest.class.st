Class {
	#name : #MixTest,
	#superclass : #TestCase,
	#instVars : [
		'mix',
		'topographies'
	],
	#category : #'Topography-Tests'
}

{ #category : #running }
MixTest >> setUp [
"inits the mix topography of the example"
	mix := Mix new.
	topographies := OrderedCollection
		with: Water new
		with: Earth new
		with: Water new
		with: Earth new.
	mix topographies addAll: topographies
]

{ #category : #running }
MixTest >> testAddTopographies [

	self assert: mix topographies size identicalTo: 4.
	mix
		addTopographies: (OrderedCollection with: Water new with: Earth new with: Water new).
	self assert: mix topographies size identicalTo: 7
]

{ #category : #running }
MixTest >> testAddTopography [
	self assert: mix topographies size identicalTo: 4.
	mix addTopography: Water new.
	self assert: mix topographies size identicalTo: 5.
	
]

{ #category : #tests }
MixTest >> testEqualProportion [
	| topography |
	"create a new mix topography composed with the same proportions"
	topography := Mix new topographies: topographies.
	self assert: (mix equalProportion: topography).
	self deny: (mix equalProportion: Water new).
	self deny: (mix equalProportion: Earth new).

	"check when we add the same proportion to diferents topographies"
	mix addTopography: Water new.
	topography addTopography: Water new.
	self assert: (mix equalProportion: topography)
]

{ #category : #running }
MixTest >> testInitialize [
	mix := Mix new.
	self assert: mix topographies class identicalTo: OrderedCollection.
	self assert: mix topographies size identicalTo: 0
]

{ #category : #tests }
MixTest >> testProportion [

	self assert: mix proportion equals: 1 / 2.
	mix topographies add: Water new.
	self assert: mix proportion equals: 3 / 5.
	mix topographies add: Water new.
	self assert: mix proportion equals: 2 / 3.
	mix topographies add: Earth new.
	self assert: mix proportion equals: 4 / 7
]
