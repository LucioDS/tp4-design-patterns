Class {
	#name : #WaterTest,
	#superclass : #TestCase,
	#instVars : [
		'water'
	],
	#category : #'Topography-Tests'
}

{ #category : #running }
WaterTest >> setUp [
	water := Water new
]

{ #category : #running }
WaterTest >> testProportion [
	self assert: water proportion equals: 1
]
