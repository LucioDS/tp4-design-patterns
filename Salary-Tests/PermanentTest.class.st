Class {
	#name : #PermanentTest,
	#superclass : #TestCase,
	#instVars : [
		'permanent'
	],
	#category : #'Salary-Tests'
}

{ #category : #running }
PermanentTest >> setUp [
	"permanent plant employed with 2 childs, married and 8 years working in the industry"

	permanent := Permanent newWith: 2 laborOld: 8 isMarried: true
]

{ #category : #running }
PermanentTest >> testAditional [
	"500 beacuse is married"

	self
		assert: permanent aditional
		equals: 500 + (100 * permanent amountChilds) + (100 * permanent laborOld).

	"so, we unmarry him/her to reduce those 500"
	permanent isMarried: false.
	self
		assert: permanent aditional
		equals: (100 * permanent amountChilds) + (100 * permanent laborOld)
]

{ #category : #running }
PermanentTest >> testBasic [
	self
		assert: permanent basic equals: 3000
]
