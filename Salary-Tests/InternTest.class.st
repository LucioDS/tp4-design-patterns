Class {
	#name : #InternTest,
	#superclass : #TestCase,
	#instVars : [
		'intern'
	],
	#category : #'Salary-Tests'
}

{ #category : #running }
InternTest >> setUp [
	intern := Intern newWith: 30
]

{ #category : #running }
InternTest >> testAditional [
	self assert: intern aditional equals: 100 * (intern examsDone)
]

{ #category : #running }
InternTest >> testBasic [
	self assert: intern basic equals: 2000
]
