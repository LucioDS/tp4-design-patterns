Class {
	#name : #EmployeeTest,
	#superclass : #TestCase,
	#instVars : [
		'permanent',
		'intern',
		'temporary'
	],
	#category : #'Salary-Tests'
}

{ #category : #running }
EmployeeTest >> setUp [
	permanent := Permanent newWith: 2 laborOld: 4 isMarried: true.
	intern := Intern newWith: 4.
	temporary := Temporary newWith: 5 childs: 3  isMarried: false 
]

{ #category : #running }
EmployeeTest >> testDeductions [
	self assert: permanent deductions equals: (permanent basic * 0.13)+ (permanent aditional * 0.5)
]

{ #category : #running }
EmployeeTest >> testMarriedBonus [
	self assert: permanent marriedBonus equals: 500. 
	permanent isMarried: false.
	self assert: permanent marriedBonus equals: 0.
]

{ #category : #running }
EmployeeTest >> testSalary [
	self
		assert: permanent salary
		equals:
			3000
				+ (500 + (100 * permanent amountChilds) + (100 * permanent laborOld))
				- (permanent basic * 0.13 + (permanent aditional * 0.5)).
	self
		assert: intern salary
		equals:
			2000 + (100 * intern examsDone)
				- (intern basic * 0.13 + (intern aditional * 0.5)).
	self
		assert: temporary salary
		equals:
			4000 + (10 * temporary hoursWorked) + (100 * temporary amountChilds)
				- (temporary basic * 0.13 + (temporary aditional * 0.5))
]
