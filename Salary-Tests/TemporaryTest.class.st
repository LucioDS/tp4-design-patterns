Class {
	#name : #TemporaryTest,
	#superclass : #TestCase,
	#instVars : [
		'temporary'
	],
	#category : #'Salary-Tests'
}

{ #category : #running }
TemporaryTest >> setUp [
	temporary := Temporary newWith: 30 childs: 3 isMarried: true
]

{ #category : #running }
TemporaryTest >> testAditional [
	"500 because is married"

	self
		assert: temporary aditional
		equals: 500 + (100 * temporary amountChilds).

	"so we remove the flag"
	temporary isMarried: false.
	self
		assert: temporary aditional
		equals: (100 * temporary amountChilds)
]

{ #category : #running }
TemporaryTest >> testBasic [
	self
		assert: temporary basic
		equals: 4000 + (temporary hoursWorked * 10)
]
